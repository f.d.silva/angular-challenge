# Angular Challenge



## What is this?

Welcome!

This project is built to serve as an intro to Angular.


It builds uppon the official Getting Started Angular tutorial (https://angular.io/tutorial).
We also added some recommended reading for those who are not familiar with *(or want to recap)* html, css, javascript *(and the porpuse of javascript frameworks)*.

## Getting Started

Before going head first into the Angular Tutorial, we recommend that you read the following guides:

The web:
https://developer.mozilla.org/en-US/docs/Learn/Getting_started_with_the_web/The_web_and_web_standards

HTML:
https://developer.mozilla.org/en-US/docs/Learn/HTML/Introduction_to_HTML/Getting_started

JavaScript:
https://developer.mozilla.org/en-US/docs/Web/JavaScript/A_re-introduction_to_JavaScript


JavaScript Frameworks:
https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Client-side_JavaScript_frameworks/Introduction

(FALTA CSS)

## What You're Going to do

First, start with: https://angular.io/tutorial.

When you're done, your app should have all of the views in the following diagram:

![image_before](images/angular_challenge_start.png)



Your next task will be to refactor some code and add some views so that you end up with the following diagram:

![image_after](images/angular_challenge_final_structure.png)


## Refactoring Angular Challenge

### Heroes

Before starting with this section, you should have followed the tutorial in the last one until the end (https://angular.io/tutorial).

If you followed the tutorial, you should now have the data for each hero *(name + id)* hard-coded in two seperate locations *(mock-heroes.ts and in-memory-data.service.ts)*.

As you might have guessed, this code duplication can be improved uppon. 

First, let's start by making in-memory-data.service.ts fetch it's data from our initial mock-heroes.ts. Start by importing our mock-heroes files in **in-memory-data.service.ts**:

    import { HEROES } from './mock-heroes';


And then, change the way createDb gets it's data:

    createDb() {
        const heroes = HEROES;
        return {heroes};
    }


Since we're thinking about our data, let us also add some more attributes to our **hero.ts**:

    export interface Hero {
        id: number;
        name: string;
        ad: number;
        as: number;
        hp: number;
    }


Now that our hero interface has the hability to store Attack Damage, Attack Speed and Health Points, let's update our heroes with the corresponding values. In **mock-heroes.ts**:

    import { Hero } from './hero';

    export const HEROES: Hero[] = [
        { id: 11, name: 'Dr Nice', ad: 100, as: 1.5, hp: 1000 },
        { id: 12, name: 'Narco', ad: 50, as: 1, hp: 2500  },
        { id: 13, name: 'Bombasto', ad: 150, as: 1, hp: 700  },
        { id: 14, name: 'Celeritas', ad: 100, as: 2, hp: 800  },
        { id: 15, name: 'Magneta', ad: 90, as: 3, hp: 700  },
        { id: 16, name: 'RubberMan', ad: 70, as: 1.5, hp: 1500  },
        { id: 17, name: 'Dynama', ad: 100, as: 0.5, hp: 3000  },
        { id: 18, name: 'Dr IQ', ad: 80, as: 0.7, hp: 2000  },
        { id: 19, name: 'Magma', ad: 300, as: 0.5, hp: 800  },
        { id: 20, name: 'Tornado', ad: 30, as: 4, hp: 1000  }
    ]

### Funcional

**For this section, focus only the functional part and ignore the looks *(we'll handle css styles, buttons and such later)*.**

In this section, we're going to look at the different Angular Components and make them interact in the manner described in the previous diagram (you can check more detailed images [here](images/)). Complete the following list:

1. Your dashboard, heroes and messages components should now all be shown in different pages. Each page needs to have the ability to navigate from one to the others (check [dashboard](images/dashboard.png), [heroes](images/heroes.png) and [messages](images/messages.png)).

2. We now want to be able to add or edit a hero in it's own page. In order to do so, let's add two new components: `hero-edit` and  `add-hero`. After you've generated the components refactor the code to make them work.

3. Since we now have our two new components *(hero-edit and add-hero)*, you need to go back and change hero-detail so that it's a read-only view. Also add an "edit button" in hero-details and "add hero" in heroes.




## Dar para o Scrumify Challange!

https://developer.mozilla.org/en-US/docs/Learn/Learning_and_getting_help
